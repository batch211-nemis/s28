//CRUD Operations
/*
	-CRUD is an acronym for: Create, Read, Update, and Delete
	-Create: The create function allows users to create a new record in te database
	-Read: The read function is similar to search function. It allows users to search and retrieve specific records.
	-Update: The update function is used to modify exixting records that are on our database.
	-Delete: The delete function allows users to remove records from a database that is no longer needed.
*/

//CREATE: INSERT Documents
/*
	-The mongo shell uses a Javascript for its syntax
	-MongoDB deals with objects as its structure for documents
	-We can create documents by providing objects into our methods
	-Javascript syntax:
		object.object.method({object})
*/

//INSERT ONE
/*
	Syntax:
		db.collectionName.insertOne({object});
*/

db.users.insertOne({
	firstname: "Jane",
	lastname: "Doe",
	age: 21,
	contact: {
		phone: "87654321",
		email: "janedoe@gmail.com"
	}
});

//INSERT MANY
/*
	Syntax:
		db.collectionName.insertMany{[{objectA}, {objectB}]}
*/

db.users.insertMany([
{
	firstname: "Stephen",
	lastname: "Hawking",
	age: 76,
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
	course: ["Python", "React", "PHP"],
	department: "none"
},
{
	firstname: "Neil",
	lastname: "Armstrong",
	age: 82,
	contact: {
		phone: "87654321",
		email: "neilarmstsrong@gmail.com"
	}
	course: ["React","Laravel", "SASS"],
}
]);

//READ: FIND/RETRIEVE Documents
/*
	-The documents will be returned based on their order of storage in the collection
*/

//FIND ALL DOCUMNETS
/*
	Syntax:
		db.collectionName.find();
*/

db.users.find();

//FIND USING SINGLE PARAMETER
/*
	Syntax:
		db.collectionName.find(field: value);
*/

db.users.find({firstname: "Stephen"});

//FIND USING MULTIPLE PARAMETERS
/*
	Syntax:
		db.collectionName.find({fieldA: valueA, fieldB: valueB},);
*/

db.users.find({lastname: "Armstrong", age: 82});

//FIND + PRETTY METHOD
/*
	-The pretty method allows us to be able to view the documents returned by our terminal in a "prettier" format.
	- Syntax:
		db.collectionName.find({field:value}).pretty();
*/

db.users.find({lastname: "Armstrong", age: 82}).pretty();

/*
	MongoDB Troubleshooting Connection Issues:
	https://docs.atlas.mongodb.com/troubleshoot-connection/
	MongoDB pretty Method:
	https://docs.mongodb.com/manual/reference/method/cursor.pretty/
	MongoDB insertOne Method:
	https://docs.mongodb.com/manual/reference/method/db.collection.insertOne/
	MongoDB insertMany Method:
	https://docs.mongodb.com/manual/reference/method/db.collection.insertMany/
	MongoDB find Method:
	https://docs.mongodb.com/manual/reference/method/db.collection.find/
*/

//UPDATE: EDIT a doucment

//UPDATE ONE: Updating a single document
/*
	Syntax:
		db.collectionName.updateOne({criteria}, {$set: {field:value}});
*/

//MINI-ACTIVITY
//1. Insert initial document
db.users.insertOne({
	firstname: "Test",
	lastname: "Test",
	age: 0,
	contact: {
		phone: "00000000",
		email: "test@gmail.com"
	},
	courses: [],
	department: "none"
});


//2.Update the document
	db.users.updateOne(
	{
		firstName: "Test"
	},
	{
	$set: {
		firstname: "Bill",
		lastname: "Gates",
		age: 65,
		contact: {
		phone: "87654321",
		email: "bill@gmail.com"
	},
	courses: ["PHP", "Laravel", "SASS"],
	department: "Operations",
	status: "active"
	}
}
);

//3. Return/Read/View the document using users.find. Use pretty method.
db.users.find({firstname: "Bill"}).pretty();

//UPDATING MANY: Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set {field:value}})
*/

db.users.updateMany(
	{department: "none"},
	{
		$set: {department: "HR"}
	});

db.users.find({department: "HR"}).pretty();

//REPLACE ONE
/*
	-Replace one replaces the whole document
	-If updateOne updates specific fields, replaceOne replaces the whole document
	-If updateOne updates parts, replaceable replaces the whole document
*/

db.users.replaceOne(
	{ firstname: "Bill"},
	{
		firstname: "Bill",
		lastname: "Gates",
		age: 82,
		contact: {
			phone: "12345678",
			email: "bill@rocketmail.com"
	},
	courses: ["PHP", "Laravel", "SASS"],
	department: "Operations"
	}
);

db.users.find({firstname: "Bill"});

//DELETE: Deleting Documents
/*
	-It is good to practice soft deletion or archiving documents instead of deleting them or removing them from the system.
*/
//For our example, let us create a document that we will delete.
db.users.insertOne({
	firstname: "Test",
});

//DELETE ONE: Deleting a single document
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteOne({
	firstname: "Test"
});

db.users.find({firstname: "Test"}).pretty();

//DELETE MANY: Delete many documents
/*
	Syntax:
		db.collectionName.deleteOne({criteria})
*/

db.users.deleteMany({
	firstname: "Bill"
});

db.users.find({firstname: "Bill"}).pretty();

//DELETE ALL: Delete all documents
/*
	/*
	Syntax:
		db.collectionName.deleteOne({})
*/


//ADVANCED QUERIES
/*
	-Retrieving data with coplex data structures is also a good skill for any developer to have
	-Real world examples of data can be as complex as having two or more layers of nested objects
	-learning to query these kinds of data is also essential to ensure that we are able to retrive any information that we would need in our application
*/

//Query an embedded document
//An embedded document are those types of documents that  that contain a document inside a document.

db.users.find({
	contact: {
		phone: "87654321",
		email: "stephenhawking@gmail.com"
	}
}).pretty();

//Querying an Array with Exact Elements
db.users.find({courses:["CSS", "JavaScript", "Python"]}).pretty();

//Querying an Array without regard to order
db.users.find({courses: {$all: ["React", "Python"]}}).pretty();

//Querying an Embedded Array
db.users.insertOne({
	nameArr: [
	{
		nameA: "Juan"
	},
	{
		nameB: "Tamad"
	}
]
});
db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
}).pretty();